function getPeople() {
    sendRequest('GET',"/people",'',function (data) {
        $('#mainContent').html(data);
    });
}

function getTag() {
    sendRequest('GET',"/tags",'',function (data) {
        $('#mainContent').html(data);
    });
}

function addPerson() {
    var data=$('#add-person').serialize();
    sendRequest('POST',"/people/",data,function () {
        $('#person-create-modal').modal('hide');
        getPeople();
    });
}

function addTag() {
    var data=$('#add-tag').serialize();
    sendRequest('POST',"/tags/",data,function () {
        $('#tag-create-modal').modal('hide');
        getTag();
    });
}

function updatePerson(id) {
    var data="name="+$('#person_name_'+id).val();
    sendRequest('PUT',"/people/"+id,data,function () {
        getPeople();
    });
}

function updateTag(id) {
    var data="name="+$('#tag_name_'+id).val();
    sendRequest('PUT',"/tags/"+id,data,function () {
        getTag();
    });
}
function getPersonDetail(id) {
    sendRequest('GET',"/people/"+id+"/relations/",'',function (data) {
        $('#mainContent').html(data);
    });
}

function sendRequest(method,url,data,successFunction) {
    $.ajax({
        url: url,
        data: data,
        type: method,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:successFunction
    });
}

$('.popup').on('hidden.bs.modal', function () {
    $(this).find("input,textarea,select").val('').end();
});

