$('.tag-search').select2({
    placeholder: 'Relation',
    ajax: {
        url: 'tags/autocomplete',
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: data
            };
        }
    },
    minimumInputLength: 1

});
var personSearch=$('.person-search');
personSearch.select2({
    placeholder: 'Relative',
    ajax: {
        url: 'people/autocomplete?exception_id='+personSearch.attr('id'),
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
            return {
                results: data
            };
        },
        minimumInputLength: 1
    }
});

function createRelation(id) {
    $.ajax({
        url: 'people/'+id+'/relations',
        data: {relative_id: $('.person-search').val(),tag_id: $('.tag-search').val()},
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(){getPersonDetail(id)}

    });
}