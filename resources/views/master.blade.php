<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src={!! asset("/js/jquery.js") !!}></script>
    <script type="text/javascript" src={!! asset("/js/bootstrap.min.js") !!}></script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    @yield('head')
</head>
<body>
<meta name="csrf-token" content="{{ csrf_token() }}">
@yield('header')
@yield('body')
@yield('footer')
@include('popup')
</body>
</html>