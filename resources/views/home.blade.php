@extends('master')
@section('head')
    <script type="text/javascript" src={!! asset("/js/home.js") !!}></script>
@endsection
@section('body')
<div class="text-center"><h1>Relationship builder</h1></div>
    <hr>
<div class="col-md-6 text-center" >
    <button type="button" class="btn btn-primary" onclick="getPeople()">People</button>
    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#person-create-modal">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>
</div>
<div class="col-md-6 text-center" >
    <button type="button" class="btn btn-primary" onclick="getTag()">Tags</button>
    <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#tag-create-modal">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
    </button>
</div>

<div class="col-lg-12 col-md-12 text-center " id="mainContent" style="padding-top: 40px"></div>

@endsection