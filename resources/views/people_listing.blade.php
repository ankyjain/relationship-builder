<table class="table" style="margin: 0px auto; width: 50%;">
    <thead><tr><td colspan="2"> PEOPLE</td> </tr></thead>
@foreach($people as $person)
        <tr>
            <td><input type="text" class="form-control" id="person_name_{{$person->id}}" value="{{$person->name}}"></td>
            <td><button type="button" class="btn btn-warning" onclick="updatePerson({{$person->id}})">Update</button></td>
            <td><button type="button" class="btn btn-danger" onclick="getPersonDetail({{$person->id}})">Details</button></td>
        </tr>
@endforeach
</table>
