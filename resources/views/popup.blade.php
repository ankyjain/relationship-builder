<div class="modal fade popup" id="person-create-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Person</h4>
            </div>
            <div class="modal-body">
                <form id="add-person">
                <p><input type="text" name="name" class="form-control" placeholder="Enter name"></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addPerson()">Add</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade popup" id="tag-create-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create Tag</h4>
            </div>
            <div class="modal-body">
                <form id="add-tag">
                    <p><input type="text" name="name" class="form-control" placeholder="Enter name"></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addTag()">Add</button>
            </div>
        </div>
    </div>
</div>