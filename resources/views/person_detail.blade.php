<div>
    <h1 class="text-capitalize">{{$person->name}}</h1>
    <hr>
</div>
<div>
    <table class="table table-striped">
        <thead>
        <tr>
            <td colspan="3"><h4><span class="label label-danger">Add Relatives</span></h4></td>
        </tr>
        </thead>
        <form id="relationCreateForm">
            <tr>
                <td><select class="person-search form-control" name="people_id" id="{{$person->id}}"></select></td>
                <td><select class="tag-search form-control" name="tag_id"></select></td>
                <td>
                    <button type="button" class="btn btn-success" aria-label="Left Align"
                            onclick="createRelation({{$person->id}})">
                        <span class="glyphicon glyphicon glyphicon-user" aria-hidden="true"></span> ADD
                    </button>
                </td>
            </tr>
        </form>
    </table>
</div>


<div>
    <table class="table table-striped">
        <thead>
        <tr>
            <td colspan="2"><h4><span class="label label-danger">Relatives</span></h4></td>
        </tr>
        </thead>
        @foreach($relations as $relation)
            <tr>
                <td><strong>{{$relation->relative->name}}</strong></td>
                <td><strong>{{$relation->tag->name}}</strong></td>
            </tr>
        @endforeach
        @foreach($ancestors as $ancestor)
            <tr>
                <td><strong>{{$ancestor->person->name}}</strong></td>
                <td><strong>{{$ancestor->tag->name}}</strong></td>
            </tr>
        @endforeach

    </table>
</div>

<div>
    <hr>
    <table class="table table-striped">
        <thead>
        <tr>
            <td colspan="3"><h4><span class="label label-danger">Connection Through Relatives</span></h4></td>
        </tr>
        </thead>
        @foreach($relations as $relation)
            @foreach($relation->relative->relatives as $connection)
                <tr>
                    <td><strong>{{$connection->name}}</strong></td>
                    <td><span class="label label-success">VIA</span></td>
                    <td><strong>{{$relation->relative->name}}</strong></td>
                </tr>
            @endforeach
            @foreach($relation->relative->ancestors as $connection)
                <tr>
                    <td><strong>{{$connection->name}}</strong></td>
                    <td><span class="label label-success">VIA</span></td>
                    <td><strong>{{$relation->relative->name}}</strong></td>
                </tr>
            @endforeach
        @endforeach
        @foreach($ancestors as $ancestor)
            @foreach($ancestor->person->relatives as $connection)
                <tr>
                    <td><strong>{{$connection->name}}</strong></td>
                    <td><span class="label label-success">VIA</span></td>
                    <td><strong>{{$ancestor->person->name}}</strong></td>
                </tr>
            @endforeach
            @foreach($ancestor->person->ancestors as $connection)
                <tr>
                    <td><strong>{{$connection->name}}</strong></td>
                    <td><span class="label label-success">VIA</span></td>
                    <td><strong>{{$ancestor->person->name}}</strong></td>
                </tr>
            @endforeach
        @endforeach
    </table>
</div>
<script type="text/javascript" src={!! asset("/js/person_detail.js") !!}></script>

