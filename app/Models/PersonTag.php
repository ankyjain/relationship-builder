<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonTag extends Model
{
    protected $table='people_tags';

    protected $guarded=['id'];

    public function relative()
    {
        return $this->belongsTo(Person::class,'relative_id');
    }

    public function person()
    {
        return $this->belongsTo(Person::class,'people_id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class,'tag_id');
    }
}
