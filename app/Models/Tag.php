<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table='tags';

    protected $guarded=['id'];

    public function relatives()
    {
        return $this->belongsToMany(Person::class,'people_tags','tag_id','relative_id');
    }

    public function person()
    {
        return $this->belongsToMany(Person::class,'people_tags','tag_id','people_id');
    }
}
