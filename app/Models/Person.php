<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table='people';

    protected $guarded=['id'];

    public function relatives()
    {
        return $this->belongsToMany(Person::class,'people_tags','people_id','relative_id')->withPivot('tag_id');
    }

    public function relativeTag()
    {
        return $this->hasMany(PersonTag::class,'people_id','id')->with('relative','tag');
    }

    public function ancestors()
    {
        return $this->belongsToMany(Person::class,'people_tags','relative_id','people_id')->withPivot('tag_id');

    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class,'people_tags','people_id','tag_id');
    }
}


