<?php

namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('people_listing',['people'=>Person::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Person::create(['name'=>$request->name]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Person::where('id',$id)->update(['name'=>$request->name]);
        return  Person::where('id',$id)->first();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function autoComplete(Request $request)
    {
        $data=[];
        $query=Person::where('name','LIKE','%'.$request->q.'%');
            if($request->has('exception_id')){
                $query->where('id','!=',$request->exception_id)
                    ->whereDoesntHave('relativeTag',function($query)use($request){
                    $query->Where('relative_id',$request->exception_id);
                })->whereDoesntHave('ancestors',function($query)use($request){
                        $query->where('people_id',$request->exception_id);
                    });
            }
            $people=$query->limit(10)->get();
        foreach ($people as $person) {
            $data[]= ['id' => $person->id, 'text' => $person->name];
        }
        return $data;
    }
}
