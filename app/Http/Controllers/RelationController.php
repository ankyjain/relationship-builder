<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\PersonTag;
use Illuminate\Http\Request;

class RelationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($personId)
    {
        $person = Person::where('id', $personId)->first();
        $relations = PersonTag::where('people_id', $personId)->with(['relative.relatives' => function ($query) use ($person) {
            $query->where('relative_id', '!=', $person->id);
        }])->with(['relative.ancestors' => function ($query) use ($person) {
            $query->where('people_id', '!=', $person->id);
        }])->get();
        $ancestors = PersonTag::where('relative_id', $personId)->with(['person.relatives' => function ($query) use ($person) {
            $query->where('relative_id', '!=', $person->id);
        }])->with(['person.ancestors' => function ($query) use ($person) {
            $query->where('people_id', '!=', $person->id);
        }])->get();
        return view('person_detail', compact('person', 'relations', 'ancestors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($personId, Request $request)
    {
        return PersonTag::create([
            'people_id' => $personId,
            'relative_id' => $request->relative_id,
            'tag_id' => $request->tag_id
        ]);
    }
}
