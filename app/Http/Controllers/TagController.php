<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tag_listing',['tags'=>Tag::all()]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tag::create(['name'=>$request->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Tag::where('id',$id)->first();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Tag::where('id',$id)->update(['name'=>$request->name]);
    }


    /**
     * @param Request $request
     * @return array
     */
    public function autoComplete(Request $request)
    {
        $data=[];
        $tags=Tag::where('name','LIKE','%'.$request->q.'%')->limit(10)->get();
        foreach ($tags as $tag) {
           $data[]= ['id' => $tag->id, 'text' => $tag->name];
        }
        return $data;
    }
}
