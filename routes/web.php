<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//    var_dump($query->sql);
//});
Route::get('/', ['as'=>'home','uses'=>'HomeController@index']);

Route::get('people', ['as'=>'get.people','uses'=>'PeopleController@index']);
Route::get('people/{id}/relations', ['as'=>'get.person.relations','uses'=>'RelationController@index']);
Route::post('people/{id}/relations', ['as'=>'store.people.relations','uses'=>'RelationController@store']);
Route::post('people', ['as'=>'store.people','uses'=>'PeopleController@store']);
Route::put('people/{id}', ['as'=>'update.people','uses'=>'PeopleController@update']);
Route::get('people/autocomplete', ['as'=>'autoComplete.people','uses'=>'PeopleController@autoComplete']);


Route::get('tags', ['as'=>'get.tags','uses'=>'TagController@index']);
Route::post('tags', ['as'=>'store.tags','uses'=>'TagController@store']);
Route::put('tags/{id}', ['as'=>'update.tags','uses'=>'TagController@update']);
Route::get('tags/autocomplete', ['as'=>'autoComplete.tags','uses'=>'TagController@autoComplete']);
